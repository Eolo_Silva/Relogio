﻿using System;

namespace Relogio
{
    class Cronometro
    {
        private int startTime = 0;
        private int stopTime = 0;
        private int min = 0;
        private bool running = false;
        /// <summary>
        /// Classe que define o funcionamento de um Cronometro,
        /// que podem ser iniciado, parado e reseteado.
        /// </summary>
        #region Atributos
        private int _segundos;

        private int _minutos;

        private int _hora;
        #endregion

        #region Propriedades
        public int Segundos { get; set; }

        public int Minutos { get; set; }

        public int Hora { get; set; }

        public object Timer { get; private set; }
        #endregion

        #region Construtores
        //public Cronometro()
        //{
        //    Segundos = 0;
        //    Minutos = 0;
        //    Hora = 0;
        //}
        public Cronometro() : this(0, 0, 0) { }

        public Cronometro(int segundos, int minutos, int hora)
        {
            Segundos = segundos;
            Minutos = minutos;
            Hora = hora;
        }
        public Cronometro(Cronometro c) : this(c.Segundos, c.Minutos, c.Hora)
        {
            Hora = c.Hora;
        }
        #endregion

        #region Métodos Gerais
        public override string ToString()
        {
            return String.Format("O tempo percorrido: {0}:{1}:{2}", Hora, Minutos, Segundos);
        }
        #endregion

        #region Métodos
        public void Start()
        {
            this.startTime = System.currentTimeMillis();
            this.running = true;
        }
        public void Stop()
        {
            this.stopTime = System.currentTimeMillis();
            this.running = false;
        }
        public void Reset()
        {

        }
        public String getElapsedTime()
        {
            long elapsed;
            int sec;

            if (running)
            {
                elapsed = (System.currentTimeMillis() - startTime);
            }
            else
            {
                elapsed = (stopTime - startTime);
            }
            sec = (int)(elapsed / 1000);

            if (sec >= 60)
            {
                this.startTime = System.currentTimeMillis();
                min++;
                return Integer.toString(min) + "min 00s";
            }
            if (sec < 10)
                return Integer.toString(min) + "min 0" + Integer.toString(sec) + "s";
            else
                return Integer.toString(min) + "min " + Integer.toString(sec) + "s";
        }
        #endregion
    }
}
