﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Relogio
{
    public partial class Form1 : Form
    {
        int segundos = 0, minutos = 0, hora = 0;
        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.BackColor = Color.Transparent;
            this.TransparencyKey = Color.Transparent;
        }

        private void btnComecar_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            btnParar.Enabled = true;
            btnComecar.Enabled = false;
            btnReset.Enabled = false;
            btnSair.Enabled = false;

        }

        private void btnParar_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            btnReset.Enabled = true;
            btnParar.Enabled = false;
            btnComecar.Enabled = true;
            lblMensagem.Text = "O tempo percorrido: " + lblCronometro.Text.ToString();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            btnComecar.Enabled = true;
            btnParar.Enabled = false;
            btnReset.Enabled = false;
            btnSair.Enabled = true;
            lblCronometro.Text = "00:00:00";
            segundos = 0;
            minutos = 0;
            hora = 0;
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            segundos++;
            if(segundos==60)
            {
                minutos++;
                segundos = 0;
            }
            else if(minutos==60)
            {
                hora++;
                minutos = 0;
            }
            lblCronometro.Text = hora.ToString().PadLeft(2, '0') + ":" + minutos.ToString().PadLeft(2, '0') + ":" + segundos.ToString().PadLeft(2, '0');
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {

        }
    }
}
